# IPL PROJECT JavaScript - Summary

In this project, we are using JavaScript and write them in json format in public/output folder and solving some problems.
We are provided with a csv data file, in which we are extracting required data.

# Language and Tools

Language:- Javascript

IDE:- Visual Studio Code

##  Files and folders

1. In Server folder you can find all the Executable files.

        Best-economy-bowler-in-super-over.cjs
        extra-runs-conceded-per-team.cjs
        highest-number-of-player-of-match.cjs
        highest-time-player-dismissed.cjs
        matches-per-year.cjs
        matches-won-per-team-per-year.cjs
        strike-rate-of-batsman-per-year.cjs
        team-won-toss-and-match.cjs
        top-10-economical-bowler.cjs

2. In data folder youn can find the csv files which contain the data.

        deliveries.csv
        matches.csv

3. In public/output folder you can find the oupt of the code in json format.

        bestBowlerSuperOver.json
        extraRunPerTeam.json
        highest-number-of-player-of-match.json
        playerDismissed.json
        matchesPerYear.json
        matchesWonPerYearPerTeam.json
        strikeRate.json
        TossWinnerMatchWinner.json
        top10EconomicalBowler.json

## Libraries Installion Command

Intilize node in folder by running the following code

        npm init

## How to Run

1. Open ipl_project_javascript directory in VSCode/Terminal.
2. Run the file by following code:-

        node Best-economy-bowler-in-super-over.cjs
        node extra-runs-conceded-per-team.cjs
        node highest-number-of-player-of-match.cjs
        node highest-time-player-dismissed.cjs
        node matches-per-year.cjs
        node matches-won-per-team-per-year.cjs
        node strike-rate-of-batsman-per-year.cjs
        node team-won-toss-and-match.cjs
        node top-10-economical-bowler.cjs

