const fs = require('fs');
const csv = require('csv-parser');

// Function to read the CSV file and export the data to another module

let bestEconomicalBowlerSuperOver = {};
const bowler_runs = {};
const bowler_balls = {};
const teams_id=[];
fs.createReadStream('src/data/deliveries.csv').pipe(csv())
.on('data', (row) => {
    if (row.is_super_over!=0){
        let bowler = row.bowler
        let runs = parseInt(row.total_runs)
        if (bowler in bowler_runs){
            bowler_runs[bowler] += runs
            bowler_balls[bowler] += 1
        }
        else{
            bowler_runs[bowler] = runs
            bowler_balls[bowler] = 1
        }
    }
})

.on('end', () => {
    for (bowler in bowler_runs) {
      economy = bowler_runs[bowler] / (bowler_balls[bowler] / 6);
      bestEconomicalBowlerSuperOver[bowler] = economy;
    }
  
    // Sort bowlers by economy rate in descending order
    const sortedBowlers = Object.entries(bestEconomicalBowlerSuperOver).sort(
      ([, economyA], [, economyB]) => economyA - economyB
    );
  
    // Retrieve the best bowler for super overs
    const [bestBowler, bestEconomy] = sortedBowlers[0];
  
    const bestBowlerData = {
      bowler: bestBowler,
      economy: bestEconomy,
    };
  
    const jsonData = JSON.stringify(bestBowlerData, null, 2);
  
    // Write the JSON data to a file
    fs.writeFile(
      'src/public/output/bestBowlerSuperOver.json', jsonData, 'utf8',
      (err) => {
        if (err) {
          console.error('Error writing JSON file:', err);
          return;
        }
        console.log('Data written to JSON file successfully.');
      }
    );
});
