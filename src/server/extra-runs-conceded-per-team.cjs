const fs = require('fs');
const csv = require('csv-parser');

// Function to read the CSV file and export the data to another module

let extraRunPerTeam = {};
const teams_id =[];
fs.createReadStream('src/data/matches.csv').pipe(csv())
.on('data', (row) => {
    if (row.season=='2016'){
        teams_id.push(row.id);
    }
})
.on('end',() =>{
    fs.createReadStream('src/data/deliveries.csv').pipe(csv())
    .on('data', (row) => {
        if (teams_id.includes(row['match_id'])){
            const team = row['bowling_team']
            const runs = parseInt(row['extra_runs'])
            if(extraRunPerTeam[team]){
                extraRunPerTeam[team]+=runs;
            }
            else{
                extraRunPerTeam[team]=runs;
            }
        }
    })
    .on('end', () => {
        const jsonData = JSON.stringify(extraRunPerTeam, null, 2);
        
        // Write the JSON data to a file
        fs.writeFile('src/public/output/extraRunPerTeam.json', jsonData, 'utf8', (err) => {
        if (err) {
            console.error('Error writing JSON file:', err);
            return;
        }
        console.log('Data written to JSON file successfully.');
        });
    });
})
