const fs = require('fs');
const csv = require('csv-parser');

// Function to read the CSV file and export the data to another module
const matchesPerYear = {};
fs.createReadStream('src/data/matches.csv').pipe(csv())
.on('data', (row) => {
    const year=row.season
    if(matchesPerYear[year])
    {
        matchesPerYear[year]+=1
    }
    else{
        matchesPerYear[year]=1
    }
})

.on('end', () => {
    const jsonData = JSON.stringify(matchesPerYear,null,2);

    // Write the JSON data to a file
    fs.writeFile('src/public/output/matchesPerYear.json', jsonData, 'utf8', (err) => {
        if (err) {
            console.error('Error writing JSON file:', err);
            return;
        }
        console.log('Data written to JSON file successfully.');
    });
});
