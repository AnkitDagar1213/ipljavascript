const fs = require('fs');
const csv = require('csv-parser');

// Function to read the CSV file and export the data to another module

let top10EconomicalBowler = {};
const bowler_runs = {};
const bowler_balls = {};
const teams_id=[];
fs.createReadStream('src/data/matches.csv').pipe(csv())
.on('data', (row) => {
    if (row.season==2015){
        teams_id.push(row.id);
    }    
})

.on('end', () => {
    fs.createReadStream('src/data/deliveries.csv').pipe(csv())
    .on('data', (row) => {
        if(teams_id.includes(row['match_id'])){
            if (row.is_super_over==0){
                let bowler = row.bowler
                let runs = parseInt(row.total_runs)
                let extras = parseInt(row.extra_runs)
                let balls = 0
                if(row.wide_runs==0 && row.bye_runs==0 && row.legbye_runs==0 ){
                    balls=1
                }
                else{
                    balls=0
                }
                if (bowler in bowler_runs){
                    bowler_runs[bowler] += runs + extras
                    bowler_balls[bowler] += balls
                }
                else{
                    bowler_runs[bowler] = runs + extras
                    bowler_balls[bowler] = balls
                }
            }
        }
    })

    .on('end', () => {
        const calEconomy = (bowler) =>
            bowler_runs[bowler] / (bowler_balls[bowler] / 6);

        // Sort bowlers by economy rate
        const sortedBowlers = Object.keys(bowler_runs).sort(
            (a, b) => calEconomy(a) - calEconomy(b)
        );

        // Retrieve top 10 bowlers with their economy rates
        for (let index = 0; index < 10; index++) {
            top10EconomicalBowler[sortedBowlers[index]] = calEconomy(sortedBowlers[index]);
        }

        const jsonData = JSON.stringify(top10EconomicalBowler, null, 2);

        // Write the JSON data to a file
        fs.writeFile('src/public/output/top10EconomicalBowler.json', jsonData, 'utf8',
            (err) => {
            if (err) {
                console.error('Error writing JSON file:', err);
                return;
            }
            console.log('Data written to JSON file successfully.');
        });
    });
});
