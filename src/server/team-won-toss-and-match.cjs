const fs = require('fs');
const csv = require('csv-parser');

// Function to read the CSV file and export the data to another module

let TossWinnerMatchWinner = {};
fs.createReadStream('src/data/matches.csv').pipe(csv())
.on('data', (row) => {
    const tossWinner=row.toss_winner;
    const winner=row.winner;
    if(tossWinner == winner)
    {
        if(TossWinnerMatchWinner[winner]){
            TossWinnerMatchWinner[winner]+=1;
        }
        else{
            TossWinnerMatchWinner[winner]=1;
        }
    }
})

.on('end', () => {
    const jsonData = JSON.stringify(TossWinnerMatchWinner, null, 2);

    // Write the JSON data to a file
    fs.writeFile('src/public/output/TossWinnerMatchWinner.json', jsonData, 'utf8', (err) => {
        if (err) {
            console.error('Error writing JSON file:', err);
            return;
        }
        console.log('Data written to JSON file successfully.');
    });
});
