const fs = require('fs');
const csv = require('csv-parser');

let playerOfMatch = {};
const topPlayerOfMatchPerSeason = {};

// Read matches.csv file
fs.createReadStream('src/data/matches.csv').pipe(csv())
.on('data', (matchRow) => {
    const player = matchRow.player_of_match;
    const year = matchRow.season;

    if (!playerOfMatch[year]) {
        playerOfMatch[year] = {};
    }
    if (playerOfMatch[year][player]) {
        playerOfMatch[year][player] += 1;
    } else {
        playerOfMatch[year][player] = 1;
    }
})

.on('end', () => {
    for (const year in playerOfMatch) {
        const players = playerOfMatch[year];
        let maxAwards = 0;
        let topPlayer;

        for (const player in players) {
            const awards = players[player];
            if (awards > maxAwards) {
                maxAwards = awards;
                topPlayer = player;
            }
        }
        topPlayerOfMatchPerSeason[year] = topPlayer;
    }

    const jsonData = JSON.stringify(topPlayerOfMatchPerSeason, null, 2);

    // Write the JSON data to a file
    fs.writeFile('src/public/output/topPlayerOfMatchPerSeason.json', jsonData, 'utf8', (err) => {
        if (err) {
        console.error('Error writing JSON file:', err);
        return;
        }
        console.log('Data written to JSON file successfully.');
    });
});
