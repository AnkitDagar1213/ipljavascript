const fs = require('fs');
const csv = require('csv-parser');

// Function to read the CSV file and export the data to another module

let playerDismissed = {};
fs.createReadStream('src/data/deliveries.csv').pipe(csv())
.on('data', (row) => {

    const bowler=row.bowler;
    const player_dismissed =row.player_dismissed

    if(player_dismissed.length>0){
        if(playerDismissed[player_dismissed+" dismissed by "+bowler]==undefined){
            playerDismissed[player_dismissed+" dismissed by "+bowler]=1;
        }
        else{
            playerDismissed[player_dismissed+" dismissed by "+bowler]+=1;
        }
    }
    
})

.on('end', () => {
    let sortedList=[]
    for(let player in playerDismissed){
        sortedList.push([player,playerDismissed[player]]);
    }
    sortedList.sort((firstVal,secondVal)=>{
        return secondVal[1]-firstVal[1];
    })
    playerDismissed={};
    playerDismissed[sortedList[0][0]]=sortedList[0][1];
    const jsonData = JSON.stringify(playerDismissed, null, 2);

    // Write the JSON data to a file
    fs.writeFile('src/public/output/playerDismissed.json', jsonData, 'utf8', (err) => {
        if (err) {
            console.error('Error writing JSON file:', err);
            return;
        }
        console.log('Data written to JSON file successfully.');
    });
});
